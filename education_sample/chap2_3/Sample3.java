﻿class Car {
  private String name;
  private int price;
  
  Car() {
    name = "未定";
    price = 0;
    System.out.println("車を作成しました。");
  }
  
  public void show() {
    System.out.println(name + ":" + price);
  }
}

class Sample3 {
  public static void main(String[] args) {
    Car car1 = new Car();
    car1.show();
  }
}

