﻿class Point {

  private int x;
  private int y;
  
  public Point() {
    x = 0;
    y = 0;
  }
  
  public Point(int px, int py) {
    setX(px);
    setY(py);
  }
  
  public void setX(int px){
    if(px < 0)
      x = 0;
    else if(px > 100)
      x = 100;
    else
      x = px;
  }
  
  public void setY(int py){
    if(py < 0)
      y = 0;
    else if (py > 100)
      y = 100;
    else
      y = py;
  }
  
  public int getX(){
    return x;
  }
  
  public int getY(){
    return y;
  }
}

class Exercise {
  public static void main(String[] args) {
    Point p1;
    p1 = new Point();
    p1.setX(10);
    p1.setY(5);
    
    int px1 = p1.getX();
    int py1 = p1.getY();
    
    System.out.println("p1の座標は(" 
                      + px1 + "," + py1 
                      + ")です。");
    Point p2;
    p2 = new Point(20000,100000);
    
    int px2 = p2.getX();
    int py2 = p2.getY();
    
    System.out.println("p2の座標は(" 
                      + px2 + "," + py2 
                      + ")です。");
  }
}
