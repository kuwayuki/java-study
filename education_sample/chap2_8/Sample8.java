﻿class Car {

  public static int sum = 0;  // クラス変数
  
  private String name; // インスタンス変数
  private int price;   // インスタンス変数
  
  private Car(){
  }
  
  public Car(String n, int p) {
    name = n;
    price = p;
    sum++;
    System.out.println("車を作成しました。");
  }
  
  // インスタンスメソッド
  public void show() {
    System.out.println(name + ":" + price);
  }
  
  // クラスメソッド
  public static void showSum() {
    System.out.println(sum + "台作りました。");
  }
}

class Sample8 {
  public static void main(String[] args) {
    Car car1 = new Car("ホンタ",200);
    Car car2 = new Car("トヨダ",300);
    car1.show();
    car2.show();
    Car.showSum(); // クラスメソッドの呼び出し
  }
}
