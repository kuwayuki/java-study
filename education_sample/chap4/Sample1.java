﻿// Lookableインターフェース
interface Lookable {
  void look();
}

// Hearableインターフェース
interface Hearable {
  void hear();
}

// Talkableインターフェース
interface Talkable {
  void talk();
}

// Robotクラス
class Robot 
  implements Lookable,Hearable,Talkable {
  
  public void look() {
    System.out.println("四角い箱が見えます。");
  }
  
  public void hear() {
    System.out.println("犬の鳴き声がします。");
  }
  
  public void talk() {
    System.out.println("おはようございます。");
  }
}

class Sample1 {
  public static void main(String[] args) {
    
    Robot kid = new Robot();
    
    kid.look();
    kid.hear();
    kid.talk();
  }
}
