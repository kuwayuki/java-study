﻿interface Greet {
  void greet();
}

interface Bye extends Greet {
  void bye();
}

class Greeting implements Bye {
  public void greet() {
    System.out.println("こんにちわ!");
  }
  
  public void bye() {
    System.out.println("さようなら!");
  }
}

class Sample2 {
  public static void main(String[] args) {
    Greeting greeting = new Greeting();
    greeting.greet();
    greeting.bye();
  }
}
