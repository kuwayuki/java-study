﻿class Language {
  void hello() {
    System.out.println("Hello!"); 
  }
}

class Japanese extends Language {
  void hello() {
    System.out.println("こんにちは");
  }
}

class Italian extends Language {
  void hello() {
    System.out.println("Buona sera");
  }
}

class Chinese extends Language {
  void hello() {
    System.out.println("ｲ尓好");
  }
}

class Sample4 {
  public static void main(String[] args){
    Language[] language;
    language = new Language[4];
    
    language[0] = new Language();
    language[1] = new Japanese();
    language[2] = new Italian();
    language[3] = new Chinese();
    
    for(int i=0; i<language.length; i++) {
      language[i].hello();
    }
  }
}
