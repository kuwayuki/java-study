﻿class Animal {
  void cry() {
    System.out.println("?"); 
  }
}

class Cat extends Animal {
  void cry() {
    System.out.println("にゃ～");
  }
  
  void jump() {
    System.out.println("ぴょん！");
  }
}

class Sample3 {
  public static void main(String[] args){
    Animal ani = new Cat();
    ani.cry();
    Cat tama = new Cat();
    tama.jump();
  }
}

