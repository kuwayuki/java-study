﻿class Sample3 {
  public static void main(String[] args) {
    try {
      back(args[0]);
    }
    catch (Exception e) {
      System.out.println("コマンドライン引数がない!");
      System.out.println("例外情報:"+ e);
    }
    finally {
      System.out.println("終了します。");
    }
  }
  
  static void back(String a) throws Exception {
    System.out.println(a);
  }
}
      