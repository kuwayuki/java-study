﻿class Sample2 {
  public static void main(String[] args) {
    try {
      int[] test;
      test = new int[5];
      
      System.out.println("test[10]に値を代入するよ。");
      
      test[10] = 80;
      System.out.println("test[10]に80を代入した。");
      
    }
    catch (ArrayIndexOutOfBoundsException e){
      System.out.println("配列の要素を超えた！");
      System.out.println(e + "という例外が発生しました。");
    }
    finally {
      System.out.println("ここであと始末をします。");
    }
    System.out.println("終わり。");
  }
}
      