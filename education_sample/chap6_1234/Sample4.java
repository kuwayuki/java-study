﻿class MyException extends Exception{}

class Car {
  private String name;
  private int price;
  
  public Car() {
    name = "未定";
    price = 0;
    System.out.println("車を作成しました。");
  }
  public void setCar(String n, int p) throws MyException {
    if(p < 0){
      MyException e = new MyException();
      throw e;
    }
    else{
      name = n;
      price = p;
    }
  }
  public void show() {
    System.out.println(name + ":" + price);
  }
}

class Sample4 {
  public static void main(String[] args) {
    Car car = new Car();
    try {
      car.setCar("ボルシェ", -2000);
    }
    catch (Exception e) {
      System.out.println("例外"+ e + "が送出されました。");
    }
    car.show();
  }
}
