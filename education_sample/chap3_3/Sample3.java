﻿class Animal
{
  public void print() {
    System.out.println("動物の仲間");
  }
}

class Cat extends Animal {
  
  // オーバーライド
  public void print(){
    System.out.println("ネコ");
  }
}

class Dog extends Animal {
  
  // オーバーライド
  public void print(){
    System.out.println("犬");
  }
}

class Sample3 {
  public static void main(String[] args) {
    
    Animal a = new Animal();
    Cat cat = new Cat();
    Dog dog = new Dog();
    a.print();
    cat.print();
    dog.print();
  }
}

