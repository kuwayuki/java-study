﻿class Weather {
  int month;
  int day;
  String sky;
}

class Sample1 {
  public static void main(String[] args) {
    Weather today = new Weather();
    today.month = 5;
    today.day = 13;
    today.sky = "晴れ";
    System.out.println(today.month + "月" 
                       + today.day + "日" + today.sky);
  }
}
