﻿class Calc{
  int add(int a, int b) {
    return a + b;
  }
}

class Sample2 {
  public static void main(String[] args) {
    Calc calc = new Calc();
    System.out.println("8 + 9 = " + calc.add(3, 9));
  }
}
