﻿class Person {
  String name;
  float  tall;
  int    age;
  
  void print() {
    System.out.println("名前:" + name);
    System.out.println("身長:" + tall);
    System.out.println("年齢:" + age);
  }
}

class Members {
  public static void main(String[] args) {
    Person taro = new Person();
    taro.name = "太郎";
    taro.tall = 170.5f;
    taro.age = 24;
    
    Person hanako = new Person();
    hanako.name = "花子";
    hanako.tall = 158.0f;
    hanako.age = 23;
    
    taro.print();
    hanako.print();
  }
}