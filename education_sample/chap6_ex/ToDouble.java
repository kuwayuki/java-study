﻿class ToDouble {
  public static void main(String[] args) {
    
    double a=0.0;
    
    try {
      a = Double.parseDouble(args[0]);
    }
    catch (NumberFormatException e) {
      System.out.println("この文字列は実数に変換できません。");
    }
    catch (ArrayIndexOutOfBoundsException e) {
      System.out.println("コマンドライン引数がない!");
    }
    finally {
      System.out.println("入力値:" + a);
    }
  }
}