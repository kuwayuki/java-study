﻿class Animal {
  private String name;
  private int age;
  
  public void setAnimal(String n, int a) {
    name = n;
    age = a;
  }
}

class Cat extends Animal {
  
  public void cry(){
    System.out.println("にゃ～");
  }
  
  public void print(){
    System.out.println("名前は" + name + "です。");
    System.out.println(age + "才です。");
  }
}

class Sample2e {
  public static void main(String[] args) {
    
    Cat cat1 = new Cat();
    cat1.setAnimal("タマ",3);
    cat1.print();
    cat1.cry();
  }
}

