﻿class Animal {
  private int age;
  
  public void setAge(int a){
    age = a;
  }
  protected int getAge(){
    return age;
  }
}
  
class Dog extends Animal{
  private String name;
  
  public Dog(String s){
    name=s;
  }
  
  public void print(){
    System.out.println(name + "は" + getAge() + "才です。");
  }
  
}

class Exercise {
  public static void main(String[] args) {
    
    Dog pochi=new Dog("ポチ");
    pochi.setAge(12);
    pochi.print();
  }
}