﻿class Compare {
  public static void main(String[] args) {
    
    int a = Integer.parseInt(args[0]);
    int b = Integer.parseInt(args[1]);
    
    if(a > b)
      System.out.println(a + " は " + b + " よりも大きい。");
    else if(a < b)
      System.out.println(a + " は " + b + " よりも小さい。");
    else
      System.out.println(a + " と " + b + " は同じ大きさです。");
  }
}