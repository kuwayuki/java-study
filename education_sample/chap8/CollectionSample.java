﻿import java.util.*;

class CollectionSample {
  public static void main(String[] args) {
    Collection<String> c = new ArrayList<String>();
    c.add("みかん");
    c.add("りんご");
    c.add("ばなな");
    Iterator<String> iter = c.iterator();
    while(iter.hasNext()) {
        String s = iter.next();
        System.out.println(s);
    }
  }
}
