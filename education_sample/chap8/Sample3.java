﻿import java.util.*;
  
public class Sample3 {
  public static void main(String[] args) {
    List<Integer> list = new ArrayList<Integer>();
    list.add(11);
    list.add(22);
    list.add("33"); // ここでコンパイルエラーとなる
    list.add(44);
  }
}
