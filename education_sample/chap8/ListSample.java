﻿import java.util.*;

class ListSample {
  public static void main(String[] args) {
    List<String> list = new ArrayList<String>();
    list.add("みかん");
    list.add("りんご");
    list.add("ばなな");
    for (int i = 0; i < list.size(); i++) {
      System.out.println(list.get(i));
    }
  }
}
