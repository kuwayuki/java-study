﻿import java.util.*;

class MapSample {
  public static void main(String[] args) {
    Map<String, String> map = new HashMap<String, String>();
    map.put("orange", "みかん");
    map.put("apple", "りんご");
    map.put("banana", "ばなな");
    System.out.println(map.get("orange"));
    System.out.println(map.get("apple"));
    System.out.println(map.get("banana"));
  }
}
