﻿import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

class History {
	public static void main(String[] args) throws IOException {
		List<String> list = new ArrayList<String>();
		while (true) {
			System.out.print("文字列を入力してください：");
			BufferedReader br = new BufferedReader(new InputStreamReader(
					System.in));

			String str = br.readLine();

			if ("QUIT".equals(str)) {
				break;
			} else if ("CLEAR".equals(str)) {
				list.clear();
				continue;
			}

			list.add(str);
			if(list.size() > 3){
				list.remove(0);
			}
			for (String itemStr : list) {
				System.out.print(itemStr + " ");
			}
			System.out.println();
		}
	}
}

