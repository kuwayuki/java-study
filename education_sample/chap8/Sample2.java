﻿import java.util.*;
  
class Sample2 {
  public static void main(String[] args) {
    
    List<String> list = new ArrayList<String>();
    list.add("みかん");
    list.add("りんご");
    list.add("ばなな");

    for (int i = 0; i < list.size(); i++) {
      System.out.println(list.get(i));
    }
  }
}

// for-each文の場合
/*
    for (String fruit : list) {
      System.out.println(fruit);
    }
*/

// イテレータの場合
/*
    Iterator<String> itr = list.iterator();
    while (itr.hasNext()) {
      System.out.println(itr.next());
    }
*/