﻿class Sample1 {
  public static void main(String[] args) {
    
    try{
      double a = Double.parseDouble(args[0]);
      double b = Double.parseDouble(args[1]);
      double sum = a + b;
      System.out.println(a + " + " + b + " = " + sum);
    }
    catch(NumberFormatException e){
      System.out.println("数値に変換できませんでした。");
    }
    catch(Exception e){
      System.out.println("例外が発生しました。" + e);
    }
  }
}
