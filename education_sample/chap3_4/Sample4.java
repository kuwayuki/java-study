﻿class Animal{
  public void print() {
    System.out.print("動物の仲間");
  }
}

class Cat extends Animal {
  
  // オーバーライド
  public void print(){
    super.print();
    System.out.println("のネコです。");
  }
}

class Dog extends Animal {
  
  // オーバーライド
  public void print(){
    super.print();
    System.out.println("の犬です。");
  }
}

class Sample4 {
  public static void main(String[] args) {
    
    Cat cat = new Cat();
    Dog dog = new Dog();
    cat.print();
    dog.print();
  }
}

