﻿enum WorkTime {
  REGULAR_TIME,   // 定時
  IRREGULAR_TIME, // 定時外
  HOLIDAY         // 休日
}

class Sample6 {
  public static void main(String[] args) {

    int basicSalary = 10000; // 基本日給
    int salary = calcSalary(basicSalary, WorkTime.HOLIDAY);
    System.out.println("本日の給料は、" + salary + "円です。");
  }

  static int calcSalary(int basicSalary, WorkTime workTime) {
    double result = basicSalary;
    switch (workTime) {
      case REGULAR_TIME:
        result *= 1.000;
        break;
      case IRREGULAR_TIME:
        result *= 1.015;
        break;
      case HOLIDAY:
        result *= 1.025;
        break;
    }
    return (int) result;
  }
}
