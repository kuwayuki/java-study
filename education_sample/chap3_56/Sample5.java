﻿class Animal{
  private String name;
  private int age;
  public Animal(String n, int a) {
    name = n;
    age = a;
  }
  public void print() {
    System.out.println("名前は" + name + "です。");
    System.out.println(age + "才です。");
  }
}

class Cat extends Animal {
  public Cat(String n, int a) {
    super(n, a); // Animalクラスのコンストラクタが呼ばれる
  }
}

class Sample5 {
  public static void main(String[] args) {
    Cat cat = new Cat("タマ", 3);
    cat.print();
  }
}

