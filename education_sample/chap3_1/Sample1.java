﻿class Animal {
  public String name;
  public int age;
  public void print(){
    System.out.println("名前は" + name + "です。");
    System.out.println(age + "才です。");
  }
}

class Cat extends Animal {
  public void cry(){
    System.out.println("ニャ～");
  }
}

class Sample1 {
  public static void main(String[] args) {
    
    Cat cat1 = new Cat();
    cat1.name = "タマ";
    cat1.age = 3;
    cat1.print();
    cat1.cry();
  }
}

