﻿class Car {
  private String name;
  private int price;
  
  private Car() {
    name = "未定";
    price = 0;
    System.out.println("車を作成しました(1)。");
  }
  
  public Car(String n, int p) {
    name = n;
    price = p;
    System.out.println("車を作成しました(2)。");
  }
  
  public void show() {
    System.out.println(name + ":" + price);
  }
}

class Sample5 {
  public static void main(String[] args) {
    // Car car1 = new Car(); ← これはできない。
    Car car1 = new Car("ホンタ",200);
    car1.show();
  }
}
