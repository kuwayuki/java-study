﻿class MyException extends Exception{}

class Car {
  private String name;
  private int price;
  
  public Car() {
    name = "未定";
    price = 0;
    System.out.println("車を作成しました。");
  }
  public void setCar(String n, int p) throws MyException {
    if(p < 0){
      MyException e = new MyException();
      throw e;
    }
    else{
      name = n;
      price = p;
    }
  }
  public void show() {
    System.out.println(name + ":" + price);
  }
}

class Sample5 {
  public static void main(String[] args) throws MyException{
    Car car = new Car();
    car.setCar("ボルシェ", -2000);
    car.show();
  }
}
