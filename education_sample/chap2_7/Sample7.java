﻿class Car {

  public static int sum = 0;  // クラス変数
  
  private String name; // インスタンス変数
  private int price;   // インスタンス変数
  
  private Car(){
  }
  
  public Car(String n, int p) {
    name = n;
    price = p;
    sum++;
    System.out.println("車を作成しました。");
  }
  
  public void show() {
    System.out.println(name + ":" + price);
  }
}

class Sample7 {
  public static void main(String[] args) {
    Car car1 = new Car("ホンタ",200);
    Car car2 = new Car("トヨダ",300);
    Car car3 = new Car("ニサン",250);
    car1.show();
    car2.show();
    car3.show();
    System.out.println(Car.sum + "台作りました。");
  }
}
