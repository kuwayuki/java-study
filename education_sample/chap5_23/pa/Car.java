﻿package pa;

class Car {
  private String name;
  private int price;
  
  private Car(){
  }
  
  public Car(String n, int p) {
    name = n;
    price = p;
    System.out.println("車を作成しました。");
  }
  
  public void show() {
    System.out.println(name + ":" + price);
  }
}