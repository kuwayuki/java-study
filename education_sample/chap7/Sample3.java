﻿import java.io.*;

class Sample3 {
  public static void main(String[] args) {
    try {
      FileInputStream in = 
            new FileInputStream("file3.dat");
      int c;
      while((c = in.read()) != -1)
        System.out.print(c + " ");
      in.close();
    }
    catch (Exception e) {
      System.out.println(e);
    }
  }
}
