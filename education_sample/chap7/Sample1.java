﻿import java.io.*;

class Sample1 {
  public static void main(String[] args) {
    try {
      FileReader in = new FileReader(args[0]);
      int c;
      String s = new String();
      while((c = in.read()) != -1) {
        s = s + (char)c;
      }
      System.out.println(s);
      in.close();
    }
    catch (IOException is) {
      System.out.println("そんなファイルはありません。");
    }
    catch (Exception e) {
      System.out.println("例外が発生しました。" + e );
    }
  }
}
