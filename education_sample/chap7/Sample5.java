﻿import java.io.*;

class Sample5 {
  public static void main(String[] args) {
    try {
      FileReader in = new FileReader(args[0]);
      BufferedReader br = new BufferedReader(in);
      
      String str;
      while((str = br.readLine()) != null) {
        System.out.println(str);
      }
      in.close();
    }
    catch (Exception e) {
      System.out.println("例外が発生しました。" + e );
    }
  }
}
