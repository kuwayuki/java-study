﻿import java.io.*;

class Sample4 {
  public static void main(String[] args) {
    try {
      char a = 'A', b = 'b';
      int  c = 128;
      FileOutputStream out = 
             new FileOutputStream("file4.dat");
      
      out.write(a);
      out.write(b);
      out.write(c);
      
      out.close();
    }
    catch (Exception e) {
      System.out.println(e);
    }
  }
}
