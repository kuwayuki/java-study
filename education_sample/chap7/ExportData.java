﻿import java.io.*;

class ExportData {
  public static void main(String[] args) {
  
    try{
      FileWriter out = new FileWriter("exdata.txt");
      BufferedWriter bw = new BufferedWriter(out);
      bw.write("雨にも負けず");
      bw.newLine();
      bw.write("風にも負けず");
      bw.newLine();
      bw.close();
    }catch(Exception e){
      System.out.println("例外が発生しました。" + e);
    }
  }
}