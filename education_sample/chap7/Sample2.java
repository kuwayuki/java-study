﻿import java.io.*;

class Sample2 {
  public static void main(String[] args) {
    try {
      FileWriter out = new FileWriter("99.txt");
      
      for(int i=1; i<=9 ;i++){
        for(int j=1; j<=9 ;j++){
          out.write(i + "*" + j + "=" + (i*j) + " ");
        }
        out.write("\n");
      }
      out.close();
    }
    catch (Exception e) {
      System.out.println(e);
    }
  }
}
