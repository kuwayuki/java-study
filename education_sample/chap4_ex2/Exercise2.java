﻿interface Animal{
  public void eat();
}

interface Dog extends Animal{
  public void run();
}

interface Bird extends Animal{
  public void fly();
}

class Shepherd implements Dog{
  public void eat(){
    // eat()の実装部
    System.out.println("シェパードは犬小屋で食事をします");
  }
  public void run(){
    // run()の実装部
    System.out.println("シェパードはとても速く走ります");
  }
}

class Swan implements Bird{
  public void eat(){
    // eat()の実装部
    System.out.println("白鳥は池で食事をします");
  }
  public void fly(){
    // fly()の実装部
    System.out.println("白鳥は優雅に飛びます");
  }
}

class Exercise2{
  public static void main(String[] args){
    
    // クラスを利用する処理
    Animal[] an = new Animal[2];

    Shepherd sh = new Shepherd();
    Swan sw = new Swan();

    an[0] = sh;
    an[1] = sw;

    for(int i = 0; i < an.length; i++) {
        an[i].eat();

        if(an[i] instanceof Dog) {
            Dog d = (Dog)an[i];
            d.run();
        } else if (an[i] instanceof Bird) {
            Bird b = (Bird)an[i];
            b.fly();
        }
    }
  }
}
