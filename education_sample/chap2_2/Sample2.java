﻿class Car {
  private String name;  // 車名
  private int price;    // 価格

  public void setName(String n) {
    if(n.length() >= 4) {
      name = n;
    } else {
      System.out.println("車名は4文字以上必要です。");
    }
  }
  
  public String getName() {
    return name;
  }
  
  public void setPrice(int p) {
    if(p > 0 && p < 10000){
      price = p;
    } else {
      System.out.println(p+"は範囲外のデータです。");
    }
  }
  
  public int getPrice() {
    return price;
  }
  
}

class Sample2 {
  public static void main(String[] args) {
    
    Car car1 = new Car();
    car1.setName("メルセダス");
    car1.setPrice(1000);
    
    System.out.println(car1.getName() 
                       + ":" + car1.getPrice());
  }
}
